#' ---
#' title: We R Live 08: Introdução à Estatística Espacial II
#' author: felipe sodre
#' date: 2020-06-23
#' ---

# prepare r -------------------------------------------------------------
# packages
# Carregando pacotes
library(rgdal)

#install.packages('spatstat')
library(spatstat)
library(maptools)
library(raster)
library(sf)

library(readr)
library(dplyr)

library(geobr)

library(tmap)

# Load data ----
fogocruzado <- read_csv("./data/fogocruzado.csv") %>% 
  st_as_sf(coords = c("longitude_ocorrencia", "latitude_ocorrencia"), crs = 4326) %>% 
  st_transform(crs = 32723) %>% 
  st_sample(1000) # 1000 tiroteios aleatorios para facilitar processamento
glimpse(fogocruzado)

# Limite Municipio RJ - GeoBr
rj <- read_municipality(code_muni = 3304557) %>% 
  st_transform(crs = 32723)

# Conversion to PPP class (Convertendo para a classe PPP) ----

# de sf a classe ppp
class(rj)

# convertendo a objeto da classe "owin"
rj_win <- as.owin(rj)

# de sf a classe sp
fc_ppp <- spatstat::ppp(
  st_coordinates(fogocruzado)[,1], 
  st_coordinates(fogocruzado)[,2], 
  window = rj_win)
plot(fc_ppp)
class(fc_ppp)

# Sumario estatistico ----
summary(fc_ppp)

# Plot
plot(fc_ppp)

# Kernel ---

# Ver live 07 :)

# Second Order Analysis (Análise de segunda ordem) ---
# L-function for HPP
L <- envelope(fc_ppp, Lest, nsim = 10, verbose = T)

plot(L)

# mudando forma de visualizacao
plot(L, .-r~r)

# L-function for IPP
L_inhom <- envelope(fc_ppp, Linhom, nsim = 10, verbose = T)
plot(L_inhom, . -r ~ r)
plot(fc_ppp)

# Hipoteticos ----
# HPP
set.seed(123)
random <- st_sample(rj, 100)
random <- spatstat::ppp(
  st_coordinates(random)[,1], 
  st_coordinates(random)[,2], 
  window = rj_win)

plot(random)
plot(
  density(
    random, bw = mean(bw.scott(random))))
Lrandom <- envelope(random, Lest, nsim = 100, verbose = T)
plot(Lrandom, .-r~r)
# Inhomgeneous
Lrandom2 <- envelope(random, Linhom, nsim = 100, verbose = T)
plot(Lrandom2, .-r~r)

# regular
set.seed(123)
regular <- st_sample(rj, 100, type = "regular")
regular_ppp <- spatstat::ppp(
  st_coordinates(regular)[,1], 
  st_coordinates(regular)[,2], 
  window = rj_win)

plot(regular_ppp)

plot(density(regular_ppp, bw = mean(bw.scott(regular_ppp)))) # analise de primeira ordem
Lregular <- envelope(regular_ppp, Lest, nsim = 100, verbose = T) # analise segunda ordem homogenea
plot(Lregular, .-r~r)

# Inhomogeneous
raios = 0:4000 # exemplo
Lregular2 <- envelope(regular_ppp, Linhom, nsim = 100, r = raios, verbose = T) # analise segunda ordem inomogenea
plot(Lregular2, .-r~r, legend = F) # parametro legend = F para remover a legenda

# Usando modelos para simular processos pontuais
pp <- rpoispp(1, win=owin(c(0,10),c(0,10)))
plot(pp)
# end ---