---
title: "We R Live 03: Elaborando mapas no R (com tmap)<br><br>"
subtitle: "<br>GeoCastBrasil" 
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 19/05/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
library(geobr) # ibge limits
library(sf) # vector
library(tmap) # tematic maps
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 65%

# We R Live 03

## Tópicos
### <u><b>Introdução: 15 min.</b></u>
### 1 Desafio da LiveR 3
### 2 Pacotes a serem usados
### 3 Considerações sobre a sintaxe

--

### <u><b>Mão na massa: 30/40 min.</b></u>
### 4 Carregar dados vetoriais
### 5 Mapas estáticos usando tmap
### 6 Mapas dinâmicos (webmapping) usando tmap
### 7 Considerações finais (5 min)

---

class: inverse, middle, center

# 1 Desafio da LiveR 3

---
background-image: url(./img/map_rio_claro_classico.png)
background-size: 300px
background-position: 50% 90%

# 1 Desafio da LiveR 3

### Elaborar um <b>mapa</b> com os princiais elementos cartográticos

#### Temperos extra: 
* Organizar nosso trabalho em um projeto do RStudio;
* O Mapa será de um município mas com o limite dos municípios limítrofes;


---

# Script para essa live

<br><br><br><br><br><br><br><br>

##### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/werlive_03/werlive_03.R`]

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(img/tmap_noLogo.png)
background-size: 350px
background-position: 10% 85%

# 2 Pacotes a serem usados

---

background-image: url(img/tmap_noLogo.png), url(img/tmap_paper.png)
background-size: 350px, 450px
background-position: 10% 85%, 90% 75% 

# 2 Pacotes a serem usados
[`https://www.jstatsoft.org/article/view/v084i06`](https://www.jstatsoft.org/article/view/v084i06)

---

class: inverse, middle, center
# 3 Instalação

---
# 3 Instalação
Vá pelo clássico:
```{r, eval=FALSE}
install.packages("tmap")
```

`r icon::fa("exclamation-triangle")` Porém... Eu tive problema na instalação. [Esse post no *stakoverflow* me ajudou](https://stackoverflow.com/questions/61312125/problem-installing-tmap-r-package-ubuntu-18-04).
```{r, eval=FALSE}
devtools::install_github("r-spatial/lwgeom")
```
---

class: inverse, middle, center

# 3 Considerações sobre sintaxe

---

# 4 Considerações sobre sintaxe

O **`tmap`**, assim como o  **`ggplot2`**, baseia-se na proposta ‘grammar of graphics’ (Wilkinson and Wills 2005):
* É baseado em `layers`
```{r, eval=FALSE}
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA)
```
---

# 4 Considerações sobre sintaxe

O **`tmap`**, assim como o  **`ggplot2`**, baseia-se na proposta ‘grammar of graphics’ (Wilkinson and Wills 2005):
* É baseado em `layers`
* Separa o dado de entrada das especificações estéticas (como o dado é visualizado);
```{r, eval=FALSE}
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA)
```
---

# 4 Considerações sobre sintaxe
O **`tmap`**, assim como o  **`ggplot2`**, baseia-se na proposta ‘grammar of graphics’ (Wilkinson and Wills 2005):
* É baseado em `layers`
* Separa o dado de entrada das especificações estéticas (como o dado é visualizado);
* O elemento básico é a função `tm_shape()`, usado para especificar o dado de entrada;
* O `tm_shape` é, então, seguido por um elemento de definição estética como `tm_fill()` e/ou `tm_dots()`, dentre [**tantos**] outros; 

```{r, eval=FALSE}
ggplot() +
  geom_sf(data = rio_claro_limit, color = "black", fill = NA) +
  geom_sf(data = polygons_land_use, aes(fill = CLASSE_USO), color = NA)
# com tmap:
tm_shape(rio_claro_limit) +
  tm_borders(col = "black") +
  tm_shape(polygons_land_use) +
  tm_fill(col = "CLASSE_USO")
```
---

# 4 Considerações sobre sintaxe
O **`tmap`**, assim como o  **`ggplot2`**, baseia-se na proposta ‘grammar of graphics’ (Wilkinson and Wills 2005):
* É baseado em `layers`
* Separa o dado de entrada das especificações estéticas (como o dado é visualizado);
* O elemento básico é a função `tm_shape()`, usado para especificar o dado de entrada;
* O `tm_shape` é, então, seguido por um elemento de definição estética como `tm_fill()` e/ou `tm_dots()`, dentre [**tantos**] outros; 

> `r icon::fa("exclamation-triangle")` Não confundir o nome da função que leva o termo **shape** com shapefile. o `tm_shape` será usado mesmo se seu dado de entrada for um **raster**!

```{r,,eval=FALSE}
tm_shape(raster_entrada) + 
  tm_raster()
```
---

class: inverse, middle, center

# Nem tudo são flores `r icon::fa_skull()`

---

# Nem tudo são flores `r icon::fa_skull()`

O `tmap` leva mais tempo para rederizar que o `ggplot2` `r icon::fa_tired()`

---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)