---
date: "2020-05-19T22:00-23:15"
tags:
- r
- rspatial
- tmap
title: We R Live 03 - Elaborando mapa no R com tmap
---

{{<youtube BmlM25XQ3QA>}}

Depois de um hiato de uma semana e o Felipe de casa nova, nessa terceira live de R, vamos apresentar um dos pacotes mais versáteis e práticos para elaboração de mapas temáticos no R: "tmap"

Slides: https://werlive.netlify.app/werlive03/werlive03 

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive03/werlive03.R

Vídeo: https://youtu.be/BmlM25XQ3QA

