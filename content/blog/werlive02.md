---
date: "2020-05-05T22:00-23:00"
tags:
- r
- rspatial
- geobr
- ggplot2
title: We R Live 02 - Elaborando mapas no R
---

{{<youtube eHht0n3Ppcg>}}

Nessa segunda live de R, vamos apresentar dois grande pacotes para manipuração e apresentação de dados espaciais: o "sf" e "ggplot2".

Slides: https://werlive.netlify.app/werlive02/werlive02

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive02/werlive02.R

Vídeo: https://youtu.be/eHht0n3Ppcg
