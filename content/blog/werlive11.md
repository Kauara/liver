---
date: "2020-07-21T21:00-22:00"
tags:
- r
- florestalpackage
title: We R Live 11 - florestal - Pacote para inventário florestal no R
---

{{<youtube xOgsVywKADI>}}

Nessa semana, teremos uma live um pouco diferente. Iremos recer o Igor para falar um pouco sobre o pacote que ele criou, o "florestal". 

O pacote que ele criou calcula parâmetros de amostragem, parâmetros fitossociológicos, além do volume lenhoso a partir da equação inserida pelo usuário ou por uma das equações utilizadas no Inventário Florestal Nacional, listadas por estado e por fitofisionomia. O pacote segue os métodos descritos em Pellico e Brena (1997): https://bit.ly/2BDbHJI.

O Igor Cobelo Ferreira é engenheiro florestal pela UnB e mestrando em Recursos Naturais do Cerrado pela UEG.

Script: https://github.com/igorcobelo/florestal

Vídeo: https://youtu.be/xOgsVywKADI