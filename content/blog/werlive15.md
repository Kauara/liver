---
date: "2020-09-15T22:00-00:00"
tags:
- r
- estatistica espacial
title: We R Live 15 - Introdução à estatística Espacial V
---

Para fechar a série de lives de introdução à estatística espacial, falaremos sobre a continuação da análise de dados agregados para polígonos.



{{<youtube IeGTr7mjZIc>}}



Slides: https://werlive.netlify.app/werlive15/werlive15

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive15/werlive15.R

Vídeo: https://youtu.be/IeGTr7mjZIc