---
date: "2020-08-18T22:00-23:00"
tags:
- r
title: We R Live 13 - Introdução à estatística Espacial IV
---

{{<youtube IsOJvaWdyXE>}}

Seguindo a série de lives de introdução à estatística espacial, falaremos um pouco sobre análise de dados agregados, ou dados de área. Enfim, os famosos polígonos.

Vamos entender um pouco sobre a natureza deles e como identificar esatatísticamente se os dados observados apresentam correlação espacial, e do que se trata isso :)

Slides: https://werlive.netlify.app/werlive13/werlive13

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive13/werlive13.R

Vídeo: https://youtu.be/IsOJvaWdyXE