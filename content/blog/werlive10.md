---
date: "2020-07-14T22:00-23:00"
tags:
- r
- rspatial
- join
title: We R Live 10 - Join como associar dados à vetores
---

{{<youtube BvURukaIDM0>}}

Uma das atividades mais comuns ao manejar dados é a vinculação de duas tabelas à partir de uma campo semelhante (em geral, um identificador único).

Falaremos sobre isso e sobre o poder dos dados geográficos que nos permitem vincular dados a partir de sua localização.

Para exemplificar, vamos utilizar dados de COVID-19 para o Estado do Rio de Janeiro.

Slides: https://werlive.netlify.app/werlive10/werlive10

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive10/werlive10.R

Vídeo: https://youtu.be/BvURukaIDM0