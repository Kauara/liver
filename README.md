# Lives de R
Repositório criado para as lives da linguagem [R](https://www.r-project.org/) do canal [GeoCastBrasil](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ).

## Site
Acesse nosso site: https://werlive.netlify.app/

## Sobre
Este material está sendo elaborado por [Maurício Vancine](https://mauriciovancine.netlify.com/) e [Felipe Sodré Barros]() como uma forma de organizar os conteúdos das lives da linguagem R. 

Buscaremos manter um único estilo de programação (*coding style*) sugeridos pelo [Hadley Wickham](http://adv-r.had.co.nz/Style.html) e [Yihui Xie](https://yihui.org/formatr/). Usaremos também a versão mais recente do [The tidyverse style guide](https://style.tidyverse.org/).

A cada live, terá uma hora de duração (aproximadamente :), dividido em 15/20 min. de introdução, comentários sobre o que será apresentado; 30/40 min. de "mão na massa" e os minutos que sobrarem para considerações finais.

Deixaremos listado neste README os *links* para os principais passos/processos executados, como forma de facilitar a busca de conteúdos. 

Com relação aos dados usados, tentaremos ao máximo, usar dados abertos e de fácil acesso, focando em funções para baixar os dados utilizando a própria linguagem. 

Caso tenham dúvidas, sugestões de temas a serem explorados ou correções em nossos materiais, nos avise [abrindo uma issue](https://docs.gitlab.com/ee/user/project/issues/) neste repositório, ou vejam [como contribuir](README.md#como-contribuir). 

Caso queira saber mais sobre git/gitlab, [veja este link](https://medium.com/ekode/primeiros-passos-com-git-e-gitlab-criando-seu-primeiro-projeto-89f9001614b0).  

## Informações técnicas
Iremos utilizar o [R](https://www.r-project.org/) versão 4.0 e o [RStudio](https://rstudio.com/), no Sistema Operacional (SO) Linux, como o [Ubuntu](https://ubuntu.com/) e [Linux Mint](https://www.linuxmint.com/), entretando, esses programas também funcionam em outros SOs como Windows ou MacOS.

## Breve introdução à linguagem R
Caso você nunca tenha programado em R, considere dar uma olhada antes nesse [material em português](https://www.curso-r.com/material/) e nesse outro [material em inglês](https://education.rstudio.com/learn/).

## Lives
Listamos a seguir as lives realizadas:  

### Live 1: ["Por onde começaR?"](https://www.youtube.com/watch?v=ZORFVdwtJ1U)
1. [Ideia das lives de R - We R Live]()
2. [Papo inicial]()
3. [Instalando o R]()
4. [Instalando o RStudio]()
5. [Como o RStudio funciona?]()
6. [Pacotes]()
7. [Instalando nosso primeiro pacote]()
8. [Material (mas não muito)]()

Vídeo: https://youtu.be/ZORFVdwtJ1U <br>
Apresentação: https://werlive.netlify.app/werlive01/werlive01

### Live 2: ["VetoRes e Mapas usando ggplot2 no R"](https://www.youtube.com/watch?v=eHht0n3Ppcg)
15/20 min: Introdução
1. [Desafio da live 2]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar dados vetoriais]()
5. [Mapas usando ggplot2]()
6. [Considerações finais]()

Vídeo: https://youtu.be/eHht0n3Ppcg <br>
Apresentação: https://werlive.netlify.app/werlive_02/werlive_02 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/werlive_02/werlive_02.R

### Live 3: ["Elaborando mapas no R (com tmap)"](https://www.youtube.com/watch?v=BmlM25XQ3QA)
15 min: Introdução
1. [Desafio da live 3]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar dados vetoriais]()
5. [Mapas estáticos usando tmap]()
6. [Mapas dinâmicos (webmapping) usando tmap]()

Vídeo: https://www.youtube.com/watch?v=BmlM25XQ3QA <br>
Apresentação: https://werlive.netlify.app/werlive03/werlive03 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive03/werlive03.R

### Live 4: ["Manipulando dados raster no R"](https://www.youtube.com/watch?v=dFC9SuGLuX8)
15 min: Introdução
1. [Desafio da live 4]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar, visualizar e manejar dados matriciais (raster)]()
5. [Reclassificação do raster (Reclassify)]()
6. [Considerações finais (5 min)]()

Vídeo: https://www.youtube.com/watch?v=dFC9SuGLuX8 <br>
Apresentação: https://werlive.netlify.app/werlive04/werlive04 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive04/werlive04.R

### Live 5: ["Manipulando dados raster no R II"](https://www.youtube.com/watch?v=AKJo_Q0dsMI)
20 min: Introdução
1. [Desafio da live 5]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar, visualizar e manejar dados matriciais (raster)]()
5. [Indices espectrais]()
6. [Considerações finais (5 min)]()

Vídeo: https://www.youtube.com/watch?v=AKJo_Q0dsMI <br>
Apresentação: https://werlive.netlify.app/werlive05/werlive05 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive05/werlive05.R

### Live 6: ["Extraindo dados climáticos para pontos"](https://www.youtube.com/watch?v=-_ODMFDU6ck)
20 min: Introdução
1. [Desafio da live 6]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar, visualizar e manejar dados climáticos (raster)]()
5. [Extrair valores para pontos]()
6. [Análises estatísticas simples]()

Vídeo: https://www.youtube.com/watch?v=-_ODMFDU6ck <br>
Apresentação: https://werlive.netlify.app/werlive06/werlive06 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive06/werlive06.R

### Live 7: ["Introdução à estatística Espacial - Processos Pontuais"](https://youtu.be/fHWD4qyKj84)
30 min: Introdução
1. [Desafio da We R Lirce 07]()
2. [Pacotes a serem usados]()
3. [Considerações conceituais]()
30 min: Mão na massa
4. [Análise de primeira ordem (raster)]()

Vídeo: https://youtu.be/fHWD4qyKj84 <br>
Apresentação: https://werlive.netlify.app/werlive07/werlive07 <br>
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive07/werlive07.R

---

## Mais conteúdos
### Sites
* [Curso-R](https://www.curso-r.com/)  
* [Finding Your Way To R](https://education.rstudio.com/learn/)

### Apostilas
* [Introdução ao software estatístico R](http://professor.ufrgs.br/sites/default/files/matiasritter/files/apostila_introducao_ao_r_-_ritter_they_and_konzen.pdf)

### Livros
#### Português
* [Ciência de dados com R](https://cdr.ibpad.com.br/index.html)
* [Software R: Análise estatísticade dados utilizando umprograma livre](http://www.editorafaith.com.br/ebooks/grat/software_r.pdf)
* [R pragmático](https://curso-r.github.io/ragmatic-book)
* [Análise espacial com R](https://www.dropbox.com/s/blgtp2bmpdghol7/AnaliseEspacialComR.pdf?dl=0)

#### Inglês
* [R for Data Science](https://r4ds.had.co.nz/)
* [Advanced R](https://adv-r.hadley.nz/)
* [Hands-On Programming with R](https://rstudio-education.github.io/hopr/)
* [Efficient R programming](https://bookdown.org/csgillespie/efficientR/)
* [R Programming for Data Science](https://bookdown.org/rdpeng/rprogdatascience/)
* [Statistical Inference via Data Science](https://moderndive.com/)
* [R Graphics Cookbook](https://r-graphics.org/)
* [Fundamentals of Data Visualization](https://serialmentor.com/dataviz/)
* [Text Mining with R](https://www.tidytextmining.com/)
* [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/)
* [Geocomputation with R](https://geocompr.robinlovelace.net/)
* [Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny](http://www.paulamoraga.com/book-geospatial/)
* [The R inferno](https://www.burns-stat.com/pages/Tutor/R_inferno.pdf) :smiling_imp:

#### Cheatsheets
[RStudio Cheatsheets](https://rstudio.com/resources/cheatsheets/)

---

## Venha trocar umas ideias
[YouTube](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ)

[Instagram](https://www.instagram.com/geocastbrasil)

[Twitter](https://twitter.com/GeoCastBrasil)

[Facebook](https://www.facebook.com/GeoCastBrasil/)

## Como contribuir
Como boas práticas, faça o *fork* desse repositório, faça as modificações sugeridas e abrar um *merge request*. 
